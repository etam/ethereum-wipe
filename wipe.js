var from = "0x..."
var to = "0x..."

var gas = new BigNumber(21000);
var price = web3.eth.gasPrice;  // current average price; or set your own
//var price = 1000000000; // 1 gwei
console.log("  gas price: "+price);
var balance = eth.getBalance(from);
console.log("  balance: "+balance);
var value = balance.minus(gas.times(price));
console.log("  value: "+value);

if (value.greaterThan(0)) {
    web3.personal.unlockAccount(from, "password")
    var txn = eth.sendTransaction({from: from, to: to, gasPrice: price, gas: gas, value: value});
    console.log("  Transfer from "+from+" to "+to+": "+txn);
}
else {
    console.log("  Transfer from "+from+" to "+to+": Error: No funds available");
}
